

connect_debug_port u_ila_1/probe6 [get_nets [list {DPRAMs/DPRAMS_GENERATE[7].sCtrlSel_reg[7][0]} {DPRAMs/DPRAMS_GENERATE[7].sCtrlSel_reg[7][1]} {DPRAMs/DPRAMS_GENERATE[7].sCtrlSel_reg[7][2]}]]


create_debug_core u_ila_0 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
set_property C_DATA_DEPTH 1024 [get_debug_cores u_ila_0]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
set_property port_width 1 [get_debug_ports u_ila_0/clk]
connect_debug_port u_ila_0/clk [get_nets [list {FMC0/mgt/example_wrapper_inst/gtwiz_userclk_rx_inst/rxusrclk_in[0]}]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
set_property port_width 8 [get_debug_ports u_ila_0/probe0]
connect_debug_port u_ila_0/probe0 [get_nets [list {sMgtRxIla[3][ctrl][0]} {sMgtRxIla[3][ctrl][1]} {sMgtRxIla[3][ctrl][2]} {sMgtRxIla[3][ctrl][3]} {sMgtRxIla[3][ctrl][4]} {sMgtRxIla[3][ctrl][5]} {sMgtRxIla[3][ctrl][6]} {sMgtRxIla[3][ctrl][7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
set_property port_width 32 [get_debug_ports u_ila_0/probe1]
connect_debug_port u_ila_0/probe1 [get_nets [list {sMgtRxIla[2][data][0]} {sMgtRxIla[2][data][1]} {sMgtRxIla[2][data][2]} {sMgtRxIla[2][data][3]} {sMgtRxIla[2][data][4]} {sMgtRxIla[2][data][5]} {sMgtRxIla[2][data][6]} {sMgtRxIla[2][data][7]} {sMgtRxIla[2][data][8]} {sMgtRxIla[2][data][9]} {sMgtRxIla[2][data][10]} {sMgtRxIla[2][data][11]} {sMgtRxIla[2][data][12]} {sMgtRxIla[2][data][13]} {sMgtRxIla[2][data][14]} {sMgtRxIla[2][data][15]} {sMgtRxIla[2][data][16]} {sMgtRxIla[2][data][17]} {sMgtRxIla[2][data][18]} {sMgtRxIla[2][data][19]} {sMgtRxIla[2][data][20]} {sMgtRxIla[2][data][21]} {sMgtRxIla[2][data][22]} {sMgtRxIla[2][data][23]} {sMgtRxIla[2][data][24]} {sMgtRxIla[2][data][25]} {sMgtRxIla[2][data][26]} {sMgtRxIla[2][data][27]} {sMgtRxIla[2][data][28]} {sMgtRxIla[2][data][29]} {sMgtRxIla[2][data][30]} {sMgtRxIla[2][data][31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe2]
set_property port_width 8 [get_debug_ports u_ila_0/probe2]
connect_debug_port u_ila_0/probe2 [get_nets [list {sMgtRxIla[2][ctrl][0]} {sMgtRxIla[2][ctrl][1]} {sMgtRxIla[2][ctrl][2]} {sMgtRxIla[2][ctrl][3]} {sMgtRxIla[2][ctrl][4]} {sMgtRxIla[2][ctrl][5]} {sMgtRxIla[2][ctrl][6]} {sMgtRxIla[2][ctrl][7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe3]
set_property port_width 8 [get_debug_ports u_ila_0/probe3]
connect_debug_port u_ila_0/probe3 [get_nets [list {sMgtRxIla[0][ctrl][0]} {sMgtRxIla[0][ctrl][1]} {sMgtRxIla[0][ctrl][2]} {sMgtRxIla[0][ctrl][3]} {sMgtRxIla[0][ctrl][4]} {sMgtRxIla[0][ctrl][5]} {sMgtRxIla[0][ctrl][6]} {sMgtRxIla[0][ctrl][7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe4]
set_property port_width 32 [get_debug_ports u_ila_0/probe4]
connect_debug_port u_ila_0/probe4 [get_nets [list {sMgtRxIla[0][data][0]} {sMgtRxIla[0][data][1]} {sMgtRxIla[0][data][2]} {sMgtRxIla[0][data][3]} {sMgtRxIla[0][data][4]} {sMgtRxIla[0][data][5]} {sMgtRxIla[0][data][6]} {sMgtRxIla[0][data][7]} {sMgtRxIla[0][data][8]} {sMgtRxIla[0][data][9]} {sMgtRxIla[0][data][10]} {sMgtRxIla[0][data][11]} {sMgtRxIla[0][data][12]} {sMgtRxIla[0][data][13]} {sMgtRxIla[0][data][14]} {sMgtRxIla[0][data][15]} {sMgtRxIla[0][data][16]} {sMgtRxIla[0][data][17]} {sMgtRxIla[0][data][18]} {sMgtRxIla[0][data][19]} {sMgtRxIla[0][data][20]} {sMgtRxIla[0][data][21]} {sMgtRxIla[0][data][22]} {sMgtRxIla[0][data][23]} {sMgtRxIla[0][data][24]} {sMgtRxIla[0][data][25]} {sMgtRxIla[0][data][26]} {sMgtRxIla[0][data][27]} {sMgtRxIla[0][data][28]} {sMgtRxIla[0][data][29]} {sMgtRxIla[0][data][30]} {sMgtRxIla[0][data][31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe5]
set_property port_width 8 [get_debug_ports u_ila_0/probe5]
connect_debug_port u_ila_0/probe5 [get_nets [list {sMgtRxIla[1][ctrl][0]} {sMgtRxIla[1][ctrl][1]} {sMgtRxIla[1][ctrl][2]} {sMgtRxIla[1][ctrl][3]} {sMgtRxIla[1][ctrl][4]} {sMgtRxIla[1][ctrl][5]} {sMgtRxIla[1][ctrl][6]} {sMgtRxIla[1][ctrl][7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe6]
set_property port_width 32 [get_debug_ports u_ila_0/probe6]
connect_debug_port u_ila_0/probe6 [get_nets [list {sMgtRxIla[1][data][0]} {sMgtRxIla[1][data][1]} {sMgtRxIla[1][data][2]} {sMgtRxIla[1][data][3]} {sMgtRxIla[1][data][4]} {sMgtRxIla[1][data][5]} {sMgtRxIla[1][data][6]} {sMgtRxIla[1][data][7]} {sMgtRxIla[1][data][8]} {sMgtRxIla[1][data][9]} {sMgtRxIla[1][data][10]} {sMgtRxIla[1][data][11]} {sMgtRxIla[1][data][12]} {sMgtRxIla[1][data][13]} {sMgtRxIla[1][data][14]} {sMgtRxIla[1][data][15]} {sMgtRxIla[1][data][16]} {sMgtRxIla[1][data][17]} {sMgtRxIla[1][data][18]} {sMgtRxIla[1][data][19]} {sMgtRxIla[1][data][20]} {sMgtRxIla[1][data][21]} {sMgtRxIla[1][data][22]} {sMgtRxIla[1][data][23]} {sMgtRxIla[1][data][24]} {sMgtRxIla[1][data][25]} {sMgtRxIla[1][data][26]} {sMgtRxIla[1][data][27]} {sMgtRxIla[1][data][28]} {sMgtRxIla[1][data][29]} {sMgtRxIla[1][data][30]} {sMgtRxIla[1][data][31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe7]
set_property port_width 32 [get_debug_ports u_ila_0/probe7]
connect_debug_port u_ila_0/probe7 [get_nets [list {sMgtRxIla[3][data][0]} {sMgtRxIla[3][data][1]} {sMgtRxIla[3][data][2]} {sMgtRxIla[3][data][3]} {sMgtRxIla[3][data][4]} {sMgtRxIla[3][data][5]} {sMgtRxIla[3][data][6]} {sMgtRxIla[3][data][7]} {sMgtRxIla[3][data][8]} {sMgtRxIla[3][data][9]} {sMgtRxIla[3][data][10]} {sMgtRxIla[3][data][11]} {sMgtRxIla[3][data][12]} {sMgtRxIla[3][data][13]} {sMgtRxIla[3][data][14]} {sMgtRxIla[3][data][15]} {sMgtRxIla[3][data][16]} {sMgtRxIla[3][data][17]} {sMgtRxIla[3][data][18]} {sMgtRxIla[3][data][19]} {sMgtRxIla[3][data][20]} {sMgtRxIla[3][data][21]} {sMgtRxIla[3][data][22]} {sMgtRxIla[3][data][23]} {sMgtRxIla[3][data][24]} {sMgtRxIla[3][data][25]} {sMgtRxIla[3][data][26]} {sMgtRxIla[3][data][27]} {sMgtRxIla[3][data][28]} {sMgtRxIla[3][data][29]} {sMgtRxIla[3][data][30]} {sMgtRxIla[3][data][31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe8]
set_property port_width 8 [get_debug_ports u_ila_0/probe8]
connect_debug_port u_ila_0/probe8 [get_nets [list {sMgtRxIla[7][ctrl][0]} {sMgtRxIla[7][ctrl][1]} {sMgtRxIla[7][ctrl][2]} {sMgtRxIla[7][ctrl][3]} {sMgtRxIla[7][ctrl][4]} {sMgtRxIla[7][ctrl][5]} {sMgtRxIla[7][ctrl][6]} {sMgtRxIla[7][ctrl][7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe9]
set_property port_width 32 [get_debug_ports u_ila_0/probe9]
connect_debug_port u_ila_0/probe9 [get_nets [list {sMgtRxIla[6][data][0]} {sMgtRxIla[6][data][1]} {sMgtRxIla[6][data][2]} {sMgtRxIla[6][data][3]} {sMgtRxIla[6][data][4]} {sMgtRxIla[6][data][5]} {sMgtRxIla[6][data][6]} {sMgtRxIla[6][data][7]} {sMgtRxIla[6][data][8]} {sMgtRxIla[6][data][9]} {sMgtRxIla[6][data][10]} {sMgtRxIla[6][data][11]} {sMgtRxIla[6][data][12]} {sMgtRxIla[6][data][13]} {sMgtRxIla[6][data][14]} {sMgtRxIla[6][data][15]} {sMgtRxIla[6][data][16]} {sMgtRxIla[6][data][17]} {sMgtRxIla[6][data][18]} {sMgtRxIla[6][data][19]} {sMgtRxIla[6][data][20]} {sMgtRxIla[6][data][21]} {sMgtRxIla[6][data][22]} {sMgtRxIla[6][data][23]} {sMgtRxIla[6][data][24]} {sMgtRxIla[6][data][25]} {sMgtRxIla[6][data][26]} {sMgtRxIla[6][data][27]} {sMgtRxIla[6][data][28]} {sMgtRxIla[6][data][29]} {sMgtRxIla[6][data][30]} {sMgtRxIla[6][data][31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe10]
set_property port_width 4 [get_debug_ports u_ila_0/probe10]
connect_debug_port u_ila_0/probe10 [get_nets [list {sMgtTx[1][ctrl][4]} {sMgtTx[1][ctrl][5]} {sMgtTx[1][ctrl][6]} {sMgtTx[1][ctrl][7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe11]
set_property port_width 4 [get_debug_ports u_ila_0/probe11]
connect_debug_port u_ila_0/probe11 [get_nets [list {sMgtTx[2][ctrl][4]} {sMgtTx[2][ctrl][5]} {sMgtTx[2][ctrl][6]} {sMgtTx[2][ctrl][7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe12]
set_property port_width 4 [get_debug_ports u_ila_0/probe12]
connect_debug_port u_ila_0/probe12 [get_nets [list {sMgtTx[0][ctrl][4]} {sMgtTx[0][ctrl][5]} {sMgtTx[0][ctrl][6]} {sMgtTx[0][ctrl][7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe13]
set_property port_width 32 [get_debug_ports u_ila_0/probe13]
connect_debug_port u_ila_0/probe13 [get_nets [list {sMgtRxIla[7][data][0]} {sMgtRxIla[7][data][1]} {sMgtRxIla[7][data][2]} {sMgtRxIla[7][data][3]} {sMgtRxIla[7][data][4]} {sMgtRxIla[7][data][5]} {sMgtRxIla[7][data][6]} {sMgtRxIla[7][data][7]} {sMgtRxIla[7][data][8]} {sMgtRxIla[7][data][9]} {sMgtRxIla[7][data][10]} {sMgtRxIla[7][data][11]} {sMgtRxIla[7][data][12]} {sMgtRxIla[7][data][13]} {sMgtRxIla[7][data][14]} {sMgtRxIla[7][data][15]} {sMgtRxIla[7][data][16]} {sMgtRxIla[7][data][17]} {sMgtRxIla[7][data][18]} {sMgtRxIla[7][data][19]} {sMgtRxIla[7][data][20]} {sMgtRxIla[7][data][21]} {sMgtRxIla[7][data][22]} {sMgtRxIla[7][data][23]} {sMgtRxIla[7][data][24]} {sMgtRxIla[7][data][25]} {sMgtRxIla[7][data][26]} {sMgtRxIla[7][data][27]} {sMgtRxIla[7][data][28]} {sMgtRxIla[7][data][29]} {sMgtRxIla[7][data][30]} {sMgtRxIla[7][data][31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe14]
set_property port_width 8 [get_debug_ports u_ila_0/probe14]
connect_debug_port u_ila_0/probe14 [get_nets [list {sMgtRxIla[4][ctrl][0]} {sMgtRxIla[4][ctrl][1]} {sMgtRxIla[4][ctrl][2]} {sMgtRxIla[4][ctrl][3]} {sMgtRxIla[4][ctrl][4]} {sMgtRxIla[4][ctrl][5]} {sMgtRxIla[4][ctrl][6]} {sMgtRxIla[4][ctrl][7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe15]
set_property port_width 32 [get_debug_ports u_ila_0/probe15]
connect_debug_port u_ila_0/probe15 [get_nets [list {sMgtRxIla[4][data][0]} {sMgtRxIla[4][data][1]} {sMgtRxIla[4][data][2]} {sMgtRxIla[4][data][3]} {sMgtRxIla[4][data][4]} {sMgtRxIla[4][data][5]} {sMgtRxIla[4][data][6]} {sMgtRxIla[4][data][7]} {sMgtRxIla[4][data][8]} {sMgtRxIla[4][data][9]} {sMgtRxIla[4][data][10]} {sMgtRxIla[4][data][11]} {sMgtRxIla[4][data][12]} {sMgtRxIla[4][data][13]} {sMgtRxIla[4][data][14]} {sMgtRxIla[4][data][15]} {sMgtRxIla[4][data][16]} {sMgtRxIla[4][data][17]} {sMgtRxIla[4][data][18]} {sMgtRxIla[4][data][19]} {sMgtRxIla[4][data][20]} {sMgtRxIla[4][data][21]} {sMgtRxIla[4][data][22]} {sMgtRxIla[4][data][23]} {sMgtRxIla[4][data][24]} {sMgtRxIla[4][data][25]} {sMgtRxIla[4][data][26]} {sMgtRxIla[4][data][27]} {sMgtRxIla[4][data][28]} {sMgtRxIla[4][data][29]} {sMgtRxIla[4][data][30]} {sMgtRxIla[4][data][31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe16]
set_property port_width 8 [get_debug_ports u_ila_0/probe16]
connect_debug_port u_ila_0/probe16 [get_nets [list {sMgtRxIla[5][ctrl][0]} {sMgtRxIla[5][ctrl][1]} {sMgtRxIla[5][ctrl][2]} {sMgtRxIla[5][ctrl][3]} {sMgtRxIla[5][ctrl][4]} {sMgtRxIla[5][ctrl][5]} {sMgtRxIla[5][ctrl][6]} {sMgtRxIla[5][ctrl][7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe17]
set_property port_width 32 [get_debug_ports u_ila_0/probe17]
connect_debug_port u_ila_0/probe17 [get_nets [list {sMgtRxIla[5][data][0]} {sMgtRxIla[5][data][1]} {sMgtRxIla[5][data][2]} {sMgtRxIla[5][data][3]} {sMgtRxIla[5][data][4]} {sMgtRxIla[5][data][5]} {sMgtRxIla[5][data][6]} {sMgtRxIla[5][data][7]} {sMgtRxIla[5][data][8]} {sMgtRxIla[5][data][9]} {sMgtRxIla[5][data][10]} {sMgtRxIla[5][data][11]} {sMgtRxIla[5][data][12]} {sMgtRxIla[5][data][13]} {sMgtRxIla[5][data][14]} {sMgtRxIla[5][data][15]} {sMgtRxIla[5][data][16]} {sMgtRxIla[5][data][17]} {sMgtRxIla[5][data][18]} {sMgtRxIla[5][data][19]} {sMgtRxIla[5][data][20]} {sMgtRxIla[5][data][21]} {sMgtRxIla[5][data][22]} {sMgtRxIla[5][data][23]} {sMgtRxIla[5][data][24]} {sMgtRxIla[5][data][25]} {sMgtRxIla[5][data][26]} {sMgtRxIla[5][data][27]} {sMgtRxIla[5][data][28]} {sMgtRxIla[5][data][29]} {sMgtRxIla[5][data][30]} {sMgtRxIla[5][data][31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe18]
set_property port_width 8 [get_debug_ports u_ila_0/probe18]
connect_debug_port u_ila_0/probe18 [get_nets [list {sMgtRxIla[6][ctrl][0]} {sMgtRxIla[6][ctrl][1]} {sMgtRxIla[6][ctrl][2]} {sMgtRxIla[6][ctrl][3]} {sMgtRxIla[6][ctrl][4]} {sMgtRxIla[6][ctrl][5]} {sMgtRxIla[6][ctrl][6]} {sMgtRxIla[6][ctrl][7]}]]
create_debug_core u_ila_1 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_1]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_1]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_1]
set_property C_DATA_DEPTH 1024 [get_debug_cores u_ila_1]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_1]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_1]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_1]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_1]
set_property port_width 1 [get_debug_ports u_ila_1/clk]
connect_debug_port u_ila_1/clk [get_nets [list {FMC0/mgt/example_wrapper_inst/gtwiz_userclk_tx_inst/txusrclk_in[0]}]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe0]
set_property port_width 32 [get_debug_ports u_ila_1/probe0]
connect_debug_port u_ila_1/probe0 [get_nets [list {sMgtTx[2][data][0]} {sMgtTx[2][data][1]} {sMgtTx[2][data][2]} {sMgtTx[2][data][3]} {sMgtTx[2][data][4]} {sMgtTx[2][data][5]} {sMgtTx[2][data][6]} {sMgtTx[2][data][7]} {sMgtTx[2][data][8]} {sMgtTx[2][data][9]} {sMgtTx[2][data][10]} {sMgtTx[2][data][11]} {sMgtTx[2][data][12]} {sMgtTx[2][data][13]} {sMgtTx[2][data][14]} {sMgtTx[2][data][15]} {sMgtTx[2][data][16]} {sMgtTx[2][data][17]} {sMgtTx[2][data][18]} {sMgtTx[2][data][19]} {sMgtTx[2][data][20]} {sMgtTx[2][data][21]} {sMgtTx[2][data][22]} {sMgtTx[2][data][23]} {sMgtTx[2][data][24]} {sMgtTx[2][data][25]} {sMgtTx[2][data][26]} {sMgtTx[2][data][27]} {sMgtTx[2][data][28]} {sMgtTx[2][data][29]} {sMgtTx[2][data][30]} {sMgtTx[2][data][31]}]]
create_debug_port u_ila_1 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe1]
set_property port_width 32 [get_debug_ports u_ila_1/probe1]
connect_debug_port u_ila_1/probe1 [get_nets [list {sMgtTx[1][data][0]} {sMgtTx[1][data][1]} {sMgtTx[1][data][2]} {sMgtTx[1][data][3]} {sMgtTx[1][data][4]} {sMgtTx[1][data][5]} {sMgtTx[1][data][6]} {sMgtTx[1][data][7]} {sMgtTx[1][data][8]} {sMgtTx[1][data][9]} {sMgtTx[1][data][10]} {sMgtTx[1][data][11]} {sMgtTx[1][data][12]} {sMgtTx[1][data][13]} {sMgtTx[1][data][14]} {sMgtTx[1][data][15]} {sMgtTx[1][data][16]} {sMgtTx[1][data][17]} {sMgtTx[1][data][18]} {sMgtTx[1][data][19]} {sMgtTx[1][data][20]} {sMgtTx[1][data][21]} {sMgtTx[1][data][22]} {sMgtTx[1][data][23]} {sMgtTx[1][data][24]} {sMgtTx[1][data][25]} {sMgtTx[1][data][26]} {sMgtTx[1][data][27]} {sMgtTx[1][data][28]} {sMgtTx[1][data][29]} {sMgtTx[1][data][30]} {sMgtTx[1][data][31]}]]
create_debug_port u_ila_1 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe2]
set_property port_width 4 [get_debug_ports u_ila_1/probe2]
connect_debug_port u_ila_1/probe2 [get_nets [list {sMgtTx[1][ctrl][0]} {sMgtTx[1][ctrl][1]} {sMgtTx[1][ctrl][2]} {sMgtTx[1][ctrl][3]}]]
create_debug_port u_ila_1 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe3]
set_property port_width 4 [get_debug_ports u_ila_1/probe3]
connect_debug_port u_ila_1/probe3 [get_nets [list {sMgtTx[2][ctrl][0]} {sMgtTx[2][ctrl][1]} {sMgtTx[2][ctrl][2]} {sMgtTx[2][ctrl][3]}]]
create_debug_port u_ila_1 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe4]
set_property port_width 32 [get_debug_ports u_ila_1/probe4]
connect_debug_port u_ila_1/probe4 [get_nets [list {sMgtTx[0][data][0]} {sMgtTx[0][data][1]} {sMgtTx[0][data][2]} {sMgtTx[0][data][3]} {sMgtTx[0][data][4]} {sMgtTx[0][data][5]} {sMgtTx[0][data][6]} {sMgtTx[0][data][7]} {sMgtTx[0][data][8]} {sMgtTx[0][data][9]} {sMgtTx[0][data][10]} {sMgtTx[0][data][11]} {sMgtTx[0][data][12]} {sMgtTx[0][data][13]} {sMgtTx[0][data][14]} {sMgtTx[0][data][15]} {sMgtTx[0][data][16]} {sMgtTx[0][data][17]} {sMgtTx[0][data][18]} {sMgtTx[0][data][19]} {sMgtTx[0][data][20]} {sMgtTx[0][data][21]} {sMgtTx[0][data][22]} {sMgtTx[0][data][23]} {sMgtTx[0][data][24]} {sMgtTx[0][data][25]} {sMgtTx[0][data][26]} {sMgtTx[0][data][27]} {sMgtTx[0][data][28]} {sMgtTx[0][data][29]} {sMgtTx[0][data][30]} {sMgtTx[0][data][31]}]]
create_debug_port u_ila_1 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe5]
set_property port_width 4 [get_debug_ports u_ila_1/probe5]
connect_debug_port u_ila_1/probe5 [get_nets [list {sMgtTx[0][ctrl][0]} {sMgtTx[0][ctrl][1]} {sMgtTx[0][ctrl][2]} {sMgtTx[0][ctrl][3]}]]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk]
