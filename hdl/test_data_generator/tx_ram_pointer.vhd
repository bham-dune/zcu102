-- Localised RAM pointer
--
-- Richard Staley, 2017/06/29

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity tx_ram_pointer is
	generic(
   DPRAM_ADDR_WIDTH: natural;
   WIB_FRAME_SIZE: natural := 120
   );
	port(
		data_clk: in STD_LOGIC;
        ram_index: out unsigned(DPRAM_ADDR_WIDTH -1 downto 0)
	);
	
end tx_ram_pointer;

architecture rtl of tx_ram_pointer is

    signal index: unsigned(DPRAM_ADDR_WIDTH-1 downto 0) := (others => '0');

begin


-- when running, index through ram array, otherwise cycle through first frame.
mgt_tick:    process(data_clk,index) 
      begin
          if rising_edge(data_clk) then
              if index =  to_unsigned(WIB_FRAME_SIZE - 1, index'length) then 
                  index <= (others => '0');
              else 
                  index <= index +1;
              end if;
          end if;
      end process;
  
     ram_index <= index after 1 ns;
	
end rtl;
