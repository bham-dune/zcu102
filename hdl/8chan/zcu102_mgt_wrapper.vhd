
-- This file wraps around the top verilog file of the example project
-- produced by running the "Open IP Example design" on the underlying IP.

library IEEE;
use IEEE.STD_LOGIC_1164.all;

use work.mgt_package.all;


entity zcu102_mgt_wrapper is
  port(
    mgtrefclk0_x1y1_n, mgtrefclk0_x1y1_p : in std_logic;

    ch0_gthrxn_in, ch0_gthrxp_in   : in  std_logic;
    ch0_gthtxn_out, ch0_gthtxp_out : out std_logic;

    ch1_gthrxn_in, ch1_gthrxp_in   : in  std_logic;
    ch1_gthtxn_out, ch1_gthtxp_out : out std_logic;

    ch2_gthrxn_in, ch2_gthrxp_in   : in  std_logic;
    ch2_gthtxn_out, ch2_gthtxp_out : out std_logic;

    ch3_gthrxn_in, ch3_gthrxp_in   : in  std_logic;
    ch3_gthtxn_out, ch3_gthtxp_out : out std_logic;

    ch4_gthrxn_in, ch4_gthrxp_in   : in  std_logic;
    ch4_gthtxn_out, ch4_gthtxp_out : out std_logic;

    ch5_gthrxn_in, ch5_gthrxp_in   : in  std_logic;
    ch5_gthtxn_out, ch5_gthtxp_out : out std_logic;

    ch6_gthrxn_in, ch6_gthrxp_in   : in  std_logic;
    ch6_gthtxn_out, ch6_gthtxp_out : out std_logic;

    ch7_gthrxn_in, ch7_gthrxp_in   : in  std_logic;
    ch7_gthtxn_out, ch7_gthtxp_out : out std_logic;

    iMGT_TX        : in  tMgtArray(7 downto 0);
    oMGT_USRCLK_TX : out std_logic;
    oMGT_STATUS_TX : out tMgtStatus;

    oMGT_RX        : out tMgtArray(7 downto 0);
    oMGT_USRCLK_RX : out std_logic;
    oMGT_STATUS_RX : out tMgtStatus;

    iMGT_INT_SEL : in std_logic;

    hb_gtwiz_reset_clk_freerun_in : in std_logic;  -- 240 MHz
    hb_gtwiz_reset_all_in         : in std_logic;

    link_down_latched_reset_in : in std_logic;

    link_status_out       : out std_logic;
    link_down_latched_out : out std_logic
    );
end zcu102_mgt_wrapper;

architecture rtl of zcu102_mgt_wrapper is
  signal sMgtTxData : std_logic_vector(255 downto 0);
  signal sMgtTxCtrl : std_logic_vector(63 downto 0);

  signal sMgtRxData : std_logic_vector(255 downto 0);
  signal sMgtRxCtrl : std_logic_vector(63 downto 0);

  component fmc0_8chan_9g6_example_top
    port (
      mgtrefclk0_x1y1_n, mgtrefclk0_x1y1_p : in std_logic;

      ch0_gthrxn_in, ch0_gthrxp_in   : in  std_logic;
      ch0_gthtxn_out, ch0_gthtxp_out : out std_logic;

      ch1_gthrxn_in, ch1_gthrxp_in   : in  std_logic;
      ch1_gthtxn_out, ch1_gthtxp_out : out std_logic;

      ch2_gthrxn_in, ch2_gthrxp_in   : in  std_logic;
      ch2_gthtxn_out, ch2_gthtxp_out : out std_logic;

      ch3_gthrxn_in, ch3_gthrxp_in   : in  std_logic;
      ch3_gthtxn_out, ch3_gthtxp_out : out std_logic;

      ch4_gthrxn_in, ch4_gthrxp_in   : in  std_logic;
      ch4_gthtxn_out, ch4_gthtxp_out : out std_logic;

      ch5_gthrxn_in, ch5_gthrxp_in   : in  std_logic;
      ch5_gthtxn_out, ch5_gthtxp_out : out std_logic;

      ch6_gthrxn_in, ch6_gthrxp_in   : in  std_logic;
      ch6_gthtxn_out, ch6_gthtxp_out : out std_logic;

      ch7_gthrxn_in, ch7_gthrxp_in   : in  std_logic;
      ch7_gthtxn_out, ch7_gthtxp_out : out std_logic;

      mgt_rx_data_out : out std_logic_vector(255 downto 0);
      mgt_rx_ctrl_out : out std_logic_vector(63 downto 0);

      mgt_rx_usrclk2_out   : out std_logic;
      rx_active_out        : out std_logic;
      rx_reset_out         : out std_logic;
      rx_reset_done_out    : out std_logic;
      rx_byteisaligned_out : out std_logic_vector(7 downto 0);

      mgt_tx_data_in : in std_logic_vector(255 downto 0);
      mgt_tx_ctrl_in : in std_logic_vector(63 downto 0);

      mgt_tx_usrclk2_out : out std_logic;
      tx_active_out      : out std_logic;
      tx_reset_out       : out std_logic;
      tx_reset_done_out  : out std_logic;

      int_sel_in : in std_logic;

      hb_gtwiz_reset_clk_freerun_in : in std_logic;  -- 240 MHz
      hb_gtwiz_reset_all_in         : in std_logic;

      link_down_latched_reset_in : in std_logic;

      link_status_out       : out std_logic;
      link_down_latched_out : out std_logic
      );
  end component;


begin

  oMGT_STATUS_TX.byteAligned <= (others => '0');  --!GTH does not have them

  connect_generate : for i in 0 to 7 generate
    sMgtTxData(31+32*i downto 32*i) <= iMGT_TX(i).data;
    sMgtTxCtrl(7+8*i downto 8*i)    <= iMGT_TX(i).ctrl;
    oMGT_RX(i).data                 <= sMgtRxData(31+32*i downto 32*i);
    oMGT_RX(i).ctrl                 <= sMgtRxCtrl(7+8*i downto 8*i);
  end generate connect_generate;


  mgt : fmc0_8chan_9g6_example_top
    port map (
      --Differential reference clock inputs
      mgtrefclk0_x1y1_p             => mgtrefclk0_x1y1_p,
      mgtrefclk0_x1y1_n             => mgtrefclk0_x1y1_n,
      --Serial data ports for transceiver channel 0
      ch0_gthrxn_in                 => ch0_gthrxn_in,
      ch0_gthrxp_in                 => ch0_gthrxp_in,
      ch0_gthtxn_out                => ch0_gthtxn_out,
      ch0_gthtxp_out                => ch0_gthtxp_out,
      --Serial data ports for transceiver channel 1
      ch1_gthrxn_in                 => ch1_gthrxn_in,
      ch1_gthrxp_in                 => ch1_gthrxp_in,
      ch1_gthtxn_out                => ch1_gthtxn_out,
      ch1_gthtxp_out                => ch1_gthtxp_out,
      --Serial data ports for transceiver channel 2
      ch2_gthrxn_in                 => ch2_gthrxn_in,
      ch2_gthrxp_in                 => ch2_gthrxp_in,
      ch2_gthtxn_out                => ch2_gthtxn_out,
      ch2_gthtxp_out                => ch2_gthtxp_out,
      --Serial data ports for transceiver channel 3
      ch3_gthrxn_in                 => ch3_gthrxn_in,
      ch3_gthrxp_in                 => ch3_gthrxp_in,
      ch3_gthtxn_out                => ch3_gthtxn_out,
      ch3_gthtxp_out                => ch3_gthtxp_out,
      --Serial data ports for transceiver channel 4
      ch4_gthrxn_in                 => ch4_gthrxn_in,
      ch4_gthrxp_in                 => ch4_gthrxp_in,
      ch4_gthtxn_out                => ch4_gthtxn_out,
      ch4_gthtxp_out                => ch4_gthtxp_out,
      --Serial data ports for transceiver channel 5
      ch5_gthrxn_in                 => ch5_gthrxn_in,
      ch5_gthrxp_in                 => ch5_gthrxp_in,
      ch5_gthtxn_out                => ch5_gthtxn_out,
      ch5_gthtxp_out                => ch5_gthtxp_out,
      --Serial data ports for transceiver channel 6
      ch6_gthrxn_in                 => ch6_gthrxn_in,
      ch6_gthrxp_in                 => ch6_gthrxp_in,
      ch6_gthtxn_out                => ch6_gthtxn_out,
      ch6_gthtxp_out                => ch6_gthtxp_out,
      --Serial data ports for transceiver channel 7
      ch7_gthrxn_in                 => ch7_gthrxn_in,
      ch7_gthrxp_in                 => ch7_gthrxp_in,
      ch7_gthtxn_out                => ch7_gthtxn_out,
      ch7_gthtxp_out                => ch7_gthtxp_out,
      --User MGT Data connection: RX
      mgt_rx_data_out               => sMgtRxData,
      mgt_rx_ctrl_out               => sMgtRxCtrl,
      mgt_rx_usrclk2_out            => oMGT_USRCLK_RX,
      rx_active_out                 => oMGT_STATUS_RX.active,
      rx_reset_out                  => oMGT_STATUS_RX.reset,
      rx_reset_done_out             => oMGT_STATUS_RX.resetDone,
      rx_byteisaligned_out          => oMGT_STATUS_RX.byteAligned,
      --User MGT Data connection: TX
      mgt_tx_data_in                => sMgtTxData,
      mgt_tx_ctrl_in                => sMgtTxCtrl,
      mgt_tx_usrclk2_out            => oMGT_USRCLK_TX,
      tx_active_out                 => oMGT_STATUS_TX.active,
      tx_reset_out                  => oMGT_STATUS_TX.reset,
      tx_reset_done_out             => oMGT_STATUS_TX.resetDone,
      --User CTRL signals
      int_sel_in                    => iMGT_INT_SEL,
      --User-provided ports for reset helper block(s)
      hb_gtwiz_reset_clk_freerun_in => hb_gtwiz_reset_clk_freerun_in,
      hb_gtwiz_reset_all_in         => hb_gtwiz_reset_all_in,
      --PRBS-based link status ports
      link_down_latched_reset_in    => link_down_latched_reset_in,
      link_status_out               => link_status_out,
      link_down_latched_out         => link_down_latched_out
      );


end rtl;
