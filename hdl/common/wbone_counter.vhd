-- Simple count_value with ipbus interface
-- count_in increments count_value. IPbus interface reads value and clears count on write
--
-- Richard Staley, 2018/09/02

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
--USE ieee.math_real.all;

use work.ipbus.all;



entity wbone_counter is
	port(
		ipbus_clk: in STD_LOGIC;
		reset: in STD_LOGIC;
		ipbus_in: in ipb_wbus;
		ipbus_out: out ipb_rbus;
		data_clk: in std_logic;
		count_in: in std_logic
	);
	
end wbone_counter;

architecture rtl of wbone_counter is


	signal count_ipbus: unsigned(31 downto 0);
	
	signal count_value: unsigned(31 downto 0);
	
	signal ack: std_logic := '0';
	
	signal ipb_clear: std_logic := '0';
	signal clear_count: std_logic_vector(1 downto 0) := (others => '0');
	signal clear_pulse: std_logic := '0';

begin

-- IPbus interface 

	process(ipbus_clk)
	begin
		if rising_edge(ipbus_clk) then
			ack <= ipbus_in.ipb_strobe and not ack;
			ipb_clear <= ipbus_in.ipb_strobe and ipbus_in.ipb_write;
		end if;
	end process;

    ipbus_out.ipb_ack <= ack;
    ipbus_out.ipb_err <= '0';

	ipbus_out.ipb_rdata <= std_logic_vector(count_ipbus) when ipbus_in.ipb_strobe = '1' else (others => '0');

-- retime clear and generate pulse

	process(data_clk, ipb_clear, clear_count )
	begin
		if rising_edge(data_clk) then 
		      clear_count(0) <= ipb_clear;
		      clear_count(1) <= clear_count(0);
			  clear_pulse <= clear_count(0) and not clear_count(1);
		end if;
    end process;

-- count

counting: process(data_clk, count_in, clear_pulse, count_value ) 
	begin
		if rising_edge(data_clk) then
			if clear_pulse = '1' then
				count_value  <=  (others => '0');
			else if count_in = '1' then
					count_value  <= count_value + 1;
				end if;
			end if;
		end if;
	end process;

-- retime count_values to ipbus domain

	process(ipbus_clk, count_value)
	begin
		if rising_edge(ipbus_clk) then 
		      count_ipbus <= count_value;
		end if;
    end process;




end rtl;
