--!@file rx_ctrl.vhd
--!@brief Rx functionalities
--!@author Mattia Barbanera, mattia.barbanera@cern.ch
--!@date 26/10/2020
--!@version 0.1 - 26/10/2020 -

--
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

use work.mgt_package.all;

--!@brief Rx functionalities
entity rx_ctrl is
  port(
    iCLK          : in  std_logic;
    oWRITING      : out std_logic_vector(cDPRAM_N-1 downto 0);
    --
    iCFG_RST      : in  std_logic_vector(cDPRAM_N-1 downto 0);  --Reset
    iCFG_EN       : in  std_logic_vector(cDPRAM_N-1 downto 0);  --Enable
    iCFG_CDISC    : in  std_logic_vector(cDPRAM_N-1 downto 0);  --Discard Commas
    iCFG_LOOP     : in  std_logic_vector(cDPRAM_N-1 downto 0);  --Infinite Loop
    iCFG_LOAD     : in  std_logic_vector(cDPRAM_N-1 downto 0);  --Load Offset
    iCFG_LOAD_VAL : in  tDpramRxAddrArray;                        --Offset
    iCFG_REPS     : in  tRepCnt;        --Reps
    --
    iMGT_DATA     : in  tMgtArray(cDPRAM_N-1 downto 0);
    --
    oRAM_WE       : out std_logic_vector(cDPRAM_N-1 downto 0);
    oRAM_ADDR     : out tDpramRxAddrArray;
    oRAM_DATA     : out tMgtArray(cDPRAM_N-1 downto 0)
    );
end rx_ctrl;

architecture std of rx_ctrl is
  signal sChRst : std_logic_vector(cDPRAM_N-1 downto 0);

  signal sAddrEn  : std_logic_vector(cDPRAM_N-1 downto 0);
  signal sAddrCnt : tDpramRxAddrArray;
  signal sAddrMax : std_logic_vector(cDPRAM_N-1 downto 0);
  signal sMaxReps : std_logic_vector(cDPRAM_N-1 downto 0);
  signal sRepsCnt : tRepCnt;

begin
  --!@brief Generate a control electronics for each dpram
  CTRL_GEN : for i in 0 to cDPRAM_N-1 generate
    sChRst(i)   <= iCFG_RST(i);
    oWRITING(i) <= iCFG_EN(i) and not sChRst(i);

    --!@brief Address counter
    ADDR_COUNTER : entity work.counter
      generic map (
        pOVERLAP  => "Y",
        pBUSWIDTH => cDPRAM_RX_ADDR_WIDTH
        ) port map (
          iCLK   => iCLK,
          iEN    => iCFG_EN(i) and sAddrEn(i),
          iRST   => sChRst(i) or sMaxReps(i),
          iLOAD  => iCFG_LOAD(i),
          iDATA  => iCFG_LOAD_VAL(i),
          oCOUNT => sAddrCnt(i),
          oMAX   => sAddrMax(i),
          oCARRY => open
          );

    --!@brief Count the repetitions number
    REPS_COUNTER : entity work.counter
      generic map (
        pOVERLAP  => "Y",
        pBUSWIDTH => cPF_REP_WIDTH
        ) port map (
          iCLK   => iCLK,
          iEN    => sAddrMax(i) and not sMaxReps(i),
          iRST   => sChRst(i),
          iLOAD  => '0',
          iDATA  => (others => '0'),
          oCOUNT => sRepsCnt(i),
          oMAX   => open,
          oCARRY => open
          );

    oRAM_ADDR(i) <= sAddrCnt(i);
    --!@brief Mux to write only the wanted characters
    COMMA_PROC : process(iCLK)
    begin
      if (rising_edge(iCLK)) then
        oRAM_DATA(i) <= iMGT_DATA(i);
        if(sChRst(i) = '1') then
          sMaxReps(i) <= '0';
          oRAM_WE(i)  <= '0';
          sAddrEn(i)  <= '0';
        else
          if (iCFG_EN(i) = '1' and
              ((sRepsCnt(i) < iCFG_REPS(i)) or iCFG_LOOP(i) = '1')
              ) then
            sMaxReps(i) <= '0';
            if(iCFG_CDISC(i) = '1' and iMGT_DATA(i).ctrl /= x"00") then
              oRAM_WE(i) <= '0';
              sAddrEn(i) <= '0';
            else  --not commas
              oRAM_WE(i) <= '1';
              sAddrEn(i) <= '1';
            end if;
          else  --en=0 or reached max reps
            sMaxReps(i) <= '1';
            oRAM_WE(i)  <= '0';
            sAddrEn(i)  <= '0';
          end if;
        end if;
      end if;
    end process COMMA_PROC;

  end generate CTRL_GEN;

end architecture;
