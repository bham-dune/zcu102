--!@file counter.vhd
--!@brief pBUSWIDTH-bit counter with load and enable.
--!@details If pOVERLAP is "N", the counter stops at its maximum value.
--!@author Mattia Barbanera, mattia.barbanera@infn.it
--!@date 09/01/2020
--!@version 1.0 - 09/01/2020 -

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;
use ieee.numeric_std.all;

entity counter is
  generic(
    pOVERLAP  : string  := "N";         --!if "N", the counter stops at its max
    pBUSWIDTH : natural := 32           --!Bit-width of the counter
    );
  port(
    iCLK   : in  std_logic;             --!Main clock
    iEN    : in  std_logic;             --!Enable
    iRST   : in  std_logic;             --!Reset
    iLOAD  : in  std_logic;             --!Load the preset value
    iDATA  : in  std_logic_vector(pBUSWIDTH-1 downto 0);  --!Preset value
    oCOUNT : out std_logic_vector(pBUSWIDTH-1 downto 0);  --!Result
    oMAX   : out std_logic;             --!Indicate that the maximum is reached
    oCARRY : out std_logic              --!Carry in case of overflow
    );
end counter;

architecture std of counter is
  signal sCount      : std_logic_vector (pBUSWIDTH-1 downto 0);
  signal sCarry      : std_logic;
  signal sEndOfCount : std_logic_vector (pBUSWIDTH-1 downto 0);

begin
  ------------------------------------------
  sEndOfCount <= (others => '1');
  sCarry      <= '1' when sCount = sEndOfCount else
            '0';
  oMAX <= sCarry;
  ------------------------------------------

  --!@brief Counter with reset, load and enable
  COUNTER_PROC : process(iCLK)
  begin
    if (rising_edge(iCLK)) then
      if(iRST = '1') then
        sCount <= (others => '0');
      else
        if(iLOAD = '1') then
          sCount <= iDATA;
        else
          if (pOVERLAP = "N") then
            if(sCarry = '1') then
              sCount <= sCount;
            else
              sCount <= sCount + iEN;
            end if;
          else
            sCount <= sCount + iEN;
          end if;
        end if;
      end if;
    end if;
  end process COUNTER_PROC;

  oCOUNT <= sCount;
  oCARRY <= sCarry;
end std;
