--!@file playback_sim.vhd
--!@brief Simulation for dprams and playback functionalities
--!@author Mattia Barbanera, mattia.barbanera@cern.ch
--!@date 12/10/2020

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

library unisim;
use unisim.VComponents.all;

library ipbus;
use ipbus.ipbus.all;
use ipbus.ipbus_reg_types.all;
use ipbus.all;

library fmc0;
use fmc0.all;
use fmc0.ipbus_decode_zcu102.all;
use fmc0.mgt_package.all;


entity playback_sim is
end entity playback_sim;

architecture behav of playback_sim is
  --Constants
  constant cCLK_TX_PERIOD : time      := 4 ns;
  constant cCLK_TX_INIT   : std_logic := '0';
  constant cCLK_RX_PERIOD : time      := 4 ns;
  constant cCLK_RX_INIT   : std_logic := '0';
  
  --IPbus signals
  signal clk_ipb         : std_logic                           := '0';
  signal rst_ipb         : std_logic                           := '0';
  signal ipb_to_slaves   : ipb_wbus_array(N_SLAVES-1 downto 0) := (others => IPB_WBUS_NULL);
  signal ipb_from_slaves : ipb_rbus_array(N_SLAVES-1 downto 0) := (others => IPB_RBUS_NULL);

  --Input File
  signal sFileRstIn     : std_logic := '1';
  signal sFileEnIn      : std_logic := '0';
  signal sFileDataCount : integer;
  signal sEof           : std_logic;
  signal sFileRst       : std_logic := '1';
  signal sFileEn        : std_logic := '0';
  signal sFileStart     : std_logic := '0';
  signal sFileLoop      : std_logic;
  signal sFileLoad      : std_logic;
  signal sFileLoadVal   : std_logic_vector(9 downto 0);
  signal sFileReps      : std_logic_vector(15 downto 0);

  --
  signal sMgtUsrclkTx : std_logic;
  signal sMgtTx       : tMgtArray(cDPRAM_N-1 downto 0);
  signal sAddr        : tDpramAddrArray;
  signal sDpramOut    : tMgtArray(cDPRAM_N-1 downto 0);

  signal sMgtUsrclkRx : std_logic;
  signal sMgtRx       : tMgtArray(cDPRAM_N-1 downto 0);
  signal sRxAddr      : tDpramAddrArray;
  signal sRxDpramWe   : std_logic_vector(cDPRAM_N-1 downto 0);
  signal sRxDpramIn   : tMgtArray(cDPRAM_N-1 downto 0);

  --
  signal sCtrlStart           : std_logic                             := '0';
  signal sCtrlSending         : std_logic_vector(cDPRAM_N-1 downto 0) := (others => '0');
  signal sCtrlRst             : std_logic_vector(cDPRAM_N-1 downto 0) := (others => '1');
  signal sCtrlEn              : std_logic_vector(cDPRAM_N-1 downto 0) := (others => '0');
  signal sCtrlLoop, sCtrlLoad : std_logic_vector(cDPRAM_N-1 downto 0) := (others => '0');
  signal sCtrlLoadVal         : tDpramAddrArray;
  signal sCtrlReps            : tRepCnt;

  signal sReadWriting : std_logic_vector(cDPRAM_N-1 downto 0);
  signal sReadRst     : std_logic_vector(cDPRAM_N-1 downto 0) := (others => '0');
  signal sReadEn      : std_logic_vector(cDPRAM_N-1 downto 0) := (others => '0');
  signal sReadLoop    : std_logic_vector(cDPRAM_N-1 downto 0) := (others => '0');
  signal sReadLoad    : std_logic_vector(cDPRAM_N-1 downto 0) := (others => '0');
  signal sReadCDisc   : std_logic_vector(cDPRAM_N-1 downto 0) := (others => '0');
  signal sReadLoadVal : tDpramAddrArray;
  signal sReadReps    : tRepCnt;

begin

  ------------------------------------------------------------------------------
  -- Combinatorial assignments
  ------------------------------------------------------------------------------
  sCtrlStart <= sFileStart;
  CONN_GENERATE : for kk in 0 to 7 generate
    sCtrlRst(kk)     <= sFileRst;
    sCtrlEn(kk)      <= sFileEn;
    sCtrlLoop(kk)    <= sFileLoop;
    sCtrlLoad(kk)    <= sFileLoad;
    sCtrlLoadVal(kk) <= sFileLoadVal;
    sCtrlReps(kk)    <= sFileReps;
    sReadRst(kk)     <= sFileRst;
    sReadEn(kk)      <= sFileEn;
    sReadLoop(kk)    <= sFileLoop;
    sReadLoad(kk)    <= sFileLoad;
    sReadCDisc(kk)   <= '0';
    sReadLoadVal(kk) <= sFileLoadVal;
    sReadReps(kk)    <= sFileReps;
  end generate CONN_GENERATE;

  ------------------------------------------------------------------------------
  -- Data generation: read the stimulus from a file and generate clock
  ------------------------------------------------------------------------------
  CLK_TX_PROC : process
    variable c : std_logic := cCLK_TX_INIT;
  begin
    wait for cCLK_TX_PERIOD/2;
    c            := not c;
    sMgtUsrclkTx <= c;
  end process CLK_TX_PROC;

  CLK_RX_PROC : process
    variable c : std_logic := cCLK_RX_INIT;
  begin
    wait for cCLK_RX_PERIOD/2;
    c            := not c;
    sMgtUsrclkRx <= c;
  end process CLK_RX_PROC;

  FILE_READ : entity work.sim_read_file
    generic map(
      pFILE => "./test.txt"
      )
    port map(
      iCLK        => sMgtUsrclkTx,
      iRST        => sFileRstIn,
      iEN         => sFileEnIn,
      oDATA_COUNT => sFileDataCount,
      oEOF        => sEof,
      oINHIBIT    => open,
      oSTART      => sFileStart,
      oRST        => sFileRst,
      oEN         => sFileEn,
      oLOOP       => sFileLoop,
      oLOAD       => sFileLoad,
      oLOAD_VAL   => sFileLoadVal,
      oREPS       => sFileReps
      );

  ------------------------------------------------------------------------------
  -- DUT: dpRAMs, playback, and readback
  ------------------------------------------------------------------------------
  TX_DPRAMS : entity fmc0.ndpram_datactrl
    port map(
      iIPB_CLK       => clk_ipb,
      iIPB_RST       => rst_ipb,
      iIPB           => ipb_to_slaves(N_SLV_DPRAMS_TX),
      oIPB           => ipb_from_slaves(N_SLV_DPRAMS_TX),
      iRAM_CLK       => sMgtUsrclkTx,
      iRAM_ADDR      => sAddr,
      oRAM_DATA_CTRL => sDpramOut
      );

  PLAYBACK_LOGIC : entity fmc0.playback_ctrl
    port map (
      iCLK          => sMgtUsrclkTx,
      iSTART        => sCtrlStart,
      oSENDING      => sCtrlSending,
      iCFG_RST      => sCtrlRst,
      iCFG_EN       => sCtrlEn,
      iCFG_LOOP     => sCtrlLoop,
      iCFG_LOAD     => sCtrlLoad,
      iCFG_LOAD_VAL => sCtrlLoadVal,
      iCFG_REPS     => sCtrlReps,
      iRAM_DATA     => sDpramOut,
      oRAM_ADDR     => sAddr,
      oMGT_DATA     => sMgtTx
      );

  --First approximation of a loopback
  sMgtRx <= sMgtTx;

  RX_DPRAMS : entity fmc0.ndpram_rx
    port map(
      iIPB_CLK       => clk_ipb,
      iIPB_RST       => rst_ipb,
      iIPB           => ipb_to_slaves(N_SLV_DPRAMS_RX),
      oIPB           => ipb_from_slaves(N_SLV_DPRAMS_RX),
      iRAM_CLK       => sMgtUsrclkRx,
      iRAM_WE        => sRxDpramWe,
      iRAM_ADDR      => sRxAddr,
      iRAM_DATA_CTRL => sRxDpramIn
      );

  READBACK_LOGIC : entity fmc0.rx_ctrl
    port map (
      iCLK          => sMgtUsrclkRx,
      oWRITING      => sReadWriting,
      iCFG_RST      => sReadRst,
      iCFG_EN       => sReadEn,
      iCFG_CDISC    => sReadCDisc,
      iCFG_LOOP     => sReadLoop,
      iCFG_LOAD     => sReadLoad,
      iCFG_LOAD_VAL => sReadLoadVal,
      iCFG_REPS     => sReadReps,
      iMGT_DATA     => sMgtRx,
      oRAM_WE       => sRxDpramWe,
      oRAM_ADDR     => sRxAddr,
      oRAM_DATA     => sRxDpramIn
      );

  ------------------------------------------------------------------------------
  -- Data checking
  ------------------------------------------------------------------------------
  -- TODO

  ------------------------------------------------------------------------------
  -- Ancillary
  ------------------------------------------------------------------------------
  --Initial commands for file reader
  INIT_PROC : process
  begin
    sFileRstIn <= '1';
    sFileEnIn  <= '0';
    wait for 10*cCLK_TX_PERIOD - 1 ns;
    sFileRstIn <= '0';
    sFileEnIn  <= '1';
    wait;
  end process INIT_PROC;

end architecture;
